var should = require('chai').should(),
temp  = require('../mazira'),
convert = temp.fahrenheitFromKelvin;

describe('#convert', function() {
	it('converts K to F;', function() {
		convert(300).should.equal(80.6);
	});
	it('checks twice;',function(){
		convert(273).should.equal(32);
	});
});






