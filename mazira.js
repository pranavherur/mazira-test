module.exports = {
  /**
   *convert kelvin to farenheit
   *param: int
   *return: int
   */
  
  fahrenheitFromKelvin: function(kelvin) {

    return ((Number(kelvin) - 273)*9/5) + 32;
  }
	
};
